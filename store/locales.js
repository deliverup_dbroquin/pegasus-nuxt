export const state = () => ({
  locales: null,
  current: null
})

export const mutations = {
  setLocales(state, locales) {
    state.locales = locales
  },
  setCurrentLocale(state, locale) {
    state.current = !!locale ? locale : state.locales[1].code
    this.app.i18n.locale = state.current
  }
}

export const getters = {
  getLocales({ locales }) {
    return locales
  },
  getCurrent({ current }) {
    return current
  }
}

export const actions = {
  async store({ commit }) {
    let locales = await this.$axios.$get("locales")
    commit("setLocales", locales)
    commit("setCurrentLocale", null)
  }
}
