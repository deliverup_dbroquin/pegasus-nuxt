import Cookie from "cookie"

export const actions = {
  async nuxtServerInit({ commit, dispatch }, { req }) {
    // Set auth
    const cookies = Cookie.parse(req.headers.cookie || "")
    if (cookies.hasOwnProperty("token")) {
      commit("auth/setToAuthenticated")
    }

    // Set locales
    await dispatch("locales/store")
  }
}
