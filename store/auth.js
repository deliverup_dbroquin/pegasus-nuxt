import Cookie from "js-cookie"

export const state = () => ({
  authenticated: false
})

export const mutations = {
  setToAuthenticated(state) {
    state.authenticated = true
  },
  setToUnauthenticated(state) {
    state.authenticated = false
  }
}

export const getters = {
  isAuthenticated({ authenticated }) {
    return authenticated
  }
}

export const actions = {
  async authenticate({ commit, dispatch }, { email, password }) {
    const access = await this.$axios.$post("oauth/token", {
      username: email,
      password,
      client_id: process.env.client,
      client_secret: process.env.secret,
      grant_type: "password"
    })

    commit("setToAuthenticated")

    dispatch("makeCookies", access)
    dispatch("refresh", access)

    this.$router.push({ path: "/" })
  },
  makeCookies({ dispatch }, response) {
    Cookie.set("token", response.access_token, {
      expires: new Date(new Date().getTime() + response.expires_in * 1000),
      secure: !this.app.context.isDev
    })

    Cookie.set("refresh", response.refresh_token)

    dispatch("updateHeaders", response)
  },
  async refresh({ dispatch, getters }, { expires_in }) {
    if (getters.isAuthenticated) {
      setTimeout(async () => {
        const refreshCookie = Cookie.get("refresh")

        if (typeof refreshCookie !== "undefined") {
          const refreshed = await this.$axios.$post("oauth/token", {
            client_id: process.env.client,
            client_secret: process.env.secret,
            refresh_token: Cookie.get("refresh"),
            grant_type: "refresh_token"
          })

          dispatch("makeCookies", refreshed)
        }
      }, expires_in)
    }
  },
  updateHeaders(ctx, { access_token, token_type }) {
    this.$axios.setToken(access_token, token_type)
  },
  logout({ commit }) {
    // Destroy cookies
    Cookie.remove("token")
    Cookie.remove("refresh")

    // Set authenticated to false
    commit("setToUnauthenticated")

    // Reset axios header
    this.$axios.setToken(false)

    // Redirect to login handled by auth middleware
    this.$router.push({ path: "/main" })
  }
}
