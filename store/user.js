export const state = () => ({
  user: false
})

export const mutations = {
  setToReduced(state) {
    state.reduced = true
  },
  setToExpanded(state) {
    state.reduced = false
  }
}

export const getters = {
  isReduced({ reduced }) {
    return reduced
  }
}

export const actions = {
  test(ctx) {
    console.log(ctx)
  }
}
