export default function ({ from, redirect, route, store }) {
    if(!store.getters['auth/isAuthenticated'] && route.fullPath !== '/login') {
        redirect('/login')
    }

    if(store.getters['auth/isAuthenticated'] && route.fullPath === '/login') {
        redirect('/')
    }
}