import Vue from "vue"
import FontAwesomeIcon from "@fortawesome/vue-fontawesome"
import fontawesome from "@fortawesome/fontawesome"
import Regular from "@fortawesome/fontawesome-free-regular"
import Solid from "@fortawesome/fontawesome-free-solid"

fontawesome.library.add(Regular)
fontawesome.library.add(Solid)

const regularIcons = []
const solidIcons = [
  { prefix: "fas", iconName: "sign-out-alt" },
  { prefix: "fas", iconName: "bars" },
  { prefix: "fas", iconName: "caret-down" }
]

const icons = [...regularIcons, ...solidIcons]

icons.forEach(icon => {
  fontawesome.icon(icon)
})

Vue.component("fa-icon", FontAwesomeIcon)
