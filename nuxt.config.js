require('dotenv').config()

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'nuxt',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Deliver Up test project using Nuxt' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  css: ['~/assets/css/main.css'],
  router: {
    middleware: ['auth', 'checkUser']
  },
  modules: [
    '@nuxtjs/axios',
    ['@nuxtjs/pwa', { Onesignal: false }],
  ],
  plugins: [
    '~/plugins/axios',
    '~/plugins/fontawesome',
    '~/plugins/i18n'
  ],
  env: {
    url: process.env.API_URL,
    client: process.env.CLIENT_ID,
    secret: process.env.CLIENT_SECRET,
    pusher: process.env.PUSHER_KEY
  },
  workbox: {
    dev: true,
    importScripts: [
      'custom-worker.js'
    ]
  },
  manifest: {
    name: process.env.APP_NAME,
    short_name: process.env.APP_SHORT_NAME,
    theme_color: process.env.APP_THEME_COLOR
  },
  /*
  ** Build configuration
  */
 build: {
  /*
   ** Vous pouvez étendre la configuration webpack ici
  */
  extend(config, ctx) {
     // Exécuter ESLint lors de la sauvegarde
     if (ctx.isDev && ctx.isClient) {
       config.module.rules.push({
         enforce: "pre",
         test: /\.(js|vue)$/,
         loader: "eslint-loader",
         exclude: /(node_modules)/
       })
     }
   }
 }
}
